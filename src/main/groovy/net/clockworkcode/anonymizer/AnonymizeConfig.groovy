package net.clockworkcode.anonymizer

class AnonymizeConfig {

    public AnonymizeConfig() {
    }

    public void load(Reader configReader, Anonymizer anonymizer) {
        Properties props = new Properties()
        props.load(configReader)
        props.each { String key, String value ->
            if (value.startsWith("type=")) {
                anonymizer.setOptions(key, toMap(value))
            } else {
                anonymizer.setChoices(key, toList(value))
            }
        }
    }

    List<String> toList(String value) {
        return value.split(',') as List<String>
    }

    private Map<String, String> toMap(String value) {
        Map<String, String> options = [:]
        value.split(',').each { String pair ->
            final String[] keyValue = pair.split('=')
            assert (keyValue.size() == 2)
            options[keyValue[0]] = keyValue[1]
        }
        return options
    }

}
