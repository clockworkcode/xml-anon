package net.clockworkcode.anonymizer

/**
 * Created: 23/05/11
 * @author david
 */
public interface Anonymizer {
    String anonymize(String path, String element, String realValue)
    String anonymize(String path, String element, String attribute, String realValue)
    void setChoices(String path, List<String> choices)
    void setOptions(String path, Map<String,String> options)
}