package net.clockworkcode.anonymizer

import com.sun.xml.internal.stream.events.CharacterEvent

import javax.xml.namespace.NamespaceContext
import javax.xml.stream.XMLEventFactory
import javax.xml.stream.XMLEventReader
import javax.xml.stream.XMLEventWriter
import javax.xml.stream.XMLStreamException
import javax.xml.stream.events.Attribute
import javax.xml.stream.events.Characters
import javax.xml.stream.events.StartElement
import javax.xml.stream.events.XMLEvent

class AnonymizerXmlEventWriter implements XMLEventWriter {
    private XMLEventWriter writer;
    private int depth;
    private boolean previousEventWasCharacters = false;
    private boolean previousEventWasStart = false;
    private List<String> path = []
    private Anonymizer anonymizer

    public AnonymizerXmlEventWriter(XMLEventWriter writer, Anonymizer anonymizer) {
        this.writer = writer;
        this.anonymizer = anonymizer
    }

    @Override
    public void flush() throws XMLStreamException {
        writer.flush();
    }

    @Override
    public void close() throws XMLStreamException {
        writer.close();
    }

    @Override
    // TODO refactor
    public void add(XMLEvent event) throws XMLStreamException {
        if (event.isStartElement()) {
            final StartElement startElement = event as StartElement
            path += startElement.getName().getLocalPart()
            final XMLEventFactory factory = XMLEventFactory.newInstance()
            String pathString = "/" + path.join('/')
            List<Attribute> attributes = startElement.attributes.collect { Attribute attr ->
                factory.createAttribute(attr.name, anonymizer.anonymize(pathString, currentElement(path), attr.name.localPart, attr.value))
            }

            event = factory.createStartElement(
                startElement.name,
                attributes.iterator(),
                startElement.namespaces
            )
        }
        if (event.isEndElement()) {
            path.pop()
        }
        if (event.isCharacters()) {
            String pathString = "/" + path.join('/')
            Characters charEvent = event.asCharacters()
            if (!(charEvent.isWhiteSpace() || charEvent.isCData() || charEvent.isIgnorableWhiteSpace())) {
                String original = charEvent.getData()
                String anon = anonymizer.anonymize(pathString, currentElement(path), original)
                if(original != anon) {
                    event = XMLEventFactory.newInstance().createCharacters(anon)
                }
            }
        }
        writer.add (event);
    }

    private String currentElement(List<String> path) {
        return path[path.size() - 1]
    }

    private void addIndent() throws XMLStreamException {
        CharacterEvent newline = new CharacterEvent("\n");
        writer.add(newline);
        CharacterEvent space = new CharacterEvent("   ");
        for (int i = 0; i < depth; i++) {
            writer.add(space);
        }
    }

    @Override
    public void add(XMLEventReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            writer.add(reader.nextEvent());
        }
    }

    @Override
    public String getPrefix(String uri) throws XMLStreamException {
        return writer.getPrefix(uri);
    }

    @Override
    public void setPrefix(String prefix, String uri) throws XMLStreamException {
        writer.setPrefix(prefix, uri);
    }

    @Override
    public void setDefaultNamespace(String uri) throws XMLStreamException {
        writer.setDefaultNamespace(uri);
    }

    @Override
    public void setNamespaceContext(NamespaceContext context) throws XMLStreamException {
        writer.setNamespaceContext(context);
    }

    @Override
    public NamespaceContext getNamespaceContext() {
        return writer.getNamespaceContext();
    }
}
