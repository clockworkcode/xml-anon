package net.clockworkcode.anonymizer

class DefaultAnonymizer implements Anonymizer {
    private Map<String, List<String>> choicesPaths = [:]
    private Map<String, Map<String, String>> optionsPaths = [:]
    Random generator
    private static String ALPHABET_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    private static String ALPHABET_LOWER = ALPHABET_UPPER.toLowerCase()
    private Closure generateValue
    private Closure pickValue

    public DefaultAnonymizer() {
        generator = new Random()
        generateValue = getDefaultGenerateClosure()
        pickValue = getDefaultPickClosure()
    }

    private Closure getDefaultPickClosure() {
        return { String realValue, List<String> choices ->
            int randomIndex = generator.nextInt(choices.size())
            return choices[randomIndex]
        }
    }

    private Closure getDefaultGenerateClosure() {
        return { String realValue, Map<String, String> options ->
            StringBuilder newValue = new StringBuilder()
            realValue.getChars().each { Character realChar ->
                if (realChar.isDigit()) {
                    newValue.append(generator.nextInt(10))
                } else if (realChar.isLetter()) {
                    String alphabet = realChar.isUpperCase() ? ALPHABET_UPPER : ALPHABET_LOWER
                    newValue.append(alphabet.charAt(generator.nextInt(26)))
                } else {
                    newValue.append(realChar)
                }
            }
            return newValue.toString()

        }
    }

    public String anonymize(String path, String element, String realValue) {
        return anonymizePath(path, element, null, realValue)
    }

    public String anonymize(String path, String element, String attribute, String realValue) {
        return anonymizePath("$path@$attribute", element, attribute, realValue)
    }

    private String anonymizePath(String path, String element, String attribute, String realValue) {
        List<String> choices = getChoices(path, element, attribute)
        Map<String, String> options = getOptions(path, element, attribute)
        if (options == null && choices == null) {
            return realValue
        }
        if (choices != null) {
            return pickValue(realValue, choices)
        }

        return generateValue(realValue, options)
    }

    private List<String> getChoices(String path, String element, String attribute) {
        String key = matchPathOrElementOrAttributeIgnoreCase(path, element, attribute, choicesPaths.keySet())
        return choicesPaths[key]
    }

    private Map<String, String> getOptions(String path, String element, String attribute) {
        String key = matchPathOrElementOrAttributeIgnoreCase(path, element, attribute, optionsPaths.keySet())
        return optionsPaths[key]
    }

    private def matchPathOrElementOrAttributeIgnoreCase(String path, String element, String attribute, Set<String> configPaths) {
        matchPathOrElementOrAttribute(path.toLowerCase(), element.toLowerCase(), attribute?.toLowerCase(), configPaths)
    }

    private String matchPathOrElementOrAttribute(String path, String element, String attribute, Set<String> configPaths) {
        String key = matchPath(path, configPaths)
        if (key == null) {
            key = matchElementOrAttribute(path, element, attribute, configPaths)
        }
        return key
    }

    private String matchPath(String path, Set<String> configPaths) {
        String key = null
        if (configPaths.contains(path)) {
            return path
        }
        return null
    }

    String matchElementOrAttribute(String path, String element, String attribute, Set<String> configPaths) {
        String key = null
        for (String configPath: configPaths) {
            if (configPath.contains("@")) {
                key = matchAttribute(attribute, configPath)
            } else if (configPath.contains("/")) {
                key = matchElement(element, configPath)
            } else {
                key = matchPotentialElementOrAttribute(element, attribute, configPath)
            }
            if(key) {
                break
            }
        }
        return key
    }

    String matchPotentialElementOrAttribute(String element, String attribute, String configPath) {
        if(element.matches(configPath) || attribute?.matches(configPath)) {
            return configPath
        }
        return null
    }

    private String matchElement(String element, String configPath) {
        if (("/" + element).matches(configPath)) {
            return configPath
        }
        return null
    }

    private String matchAttribute(String attribute, String configPath) {
        if (("@" + attribute).matches(configPath)) {
            return configPath
        }
        return null
    }

    public void setChoices(String path, List<String> choices) {
        choicesPaths[path.toLowerCase()] = choices
    }

    public void setOptions(String path, Map<String, String> options) {
        optionsPaths[path.toLowerCase()] = options
    }
}
