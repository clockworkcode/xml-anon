package net.clockworkcode.commandline

import org.apache.commons.io.FilenameUtils

/**
 * Created: 27/05/11
 * @author david
 */
class CliUtil {

    /**
     * Create an output file name based on the three file inputs from the OptionAccessor returned from CliBuilder
     * If the outputFile is set, use it
     * If the output directory is set, prepend the output file with it (if it is set
     * otherwise use the input file, see the examples below
     *
     * <table>
     *     <tr><th>inputFile</th><th>outputDir</th><th>outputFile</th><th>result</th></tr>
     *     <tr><td>inputDir/inputFile.xml</td><td>Null</td>     <td>Null</td>                 <td>inputDir/inputFile.xml</td></tr>
     *     <tr><td>inputDir/inputFile.xml</td><td>Null</td>     <td>output/outputFile.xml</td><td>output/outputFile.xml</td></tr>
     *     <tr><td>inputDir/inputFile.xml</td><td>outputDir</td><td>Null</td>                 <td>outputDir/inputFile.xml</td></tr>
     *     <tr><td>inputDir/inputFile.xml</td><td>outputDir</td><td>output/outputFile.xml</td><td>outputDir/output/outputFile.xml</td></tr>
     *     <tr><td>inputDir/inputFile.xml</td><td>outputDir</td><td>outputFile.xml</td>       <td>outputDir/outputFile.xml</td></tr>
     *     </table>
     * @param inputFile - an option from an OptionAccessor eg options.i
     * @param outputDir - an option from an OptionAccessor eg options.d
     * @param outputFile - an option from an OptionAccessor eg options.o
     * @return
     */
    public static File generateOutputFilename(def inputFileOption, def outputDirOption, def outputFileOption) {
        File inputFile
        File outputFile
        File outputDir
        File anonymizeConfig
        if (inputFileOption) {
            inputFile = new File(inputFileOption)
        }
        if (outputFileOption) {
            outputFile = new File(outputFileOption)
        }
        if (outputDirOption) {
            outputDir = new File(outputDirOption)
        }

        File output = inputFile

        if (outputFile) {
            output = outputFile

            if(outputDir) {
                output = new File(outputDir, outputFile.path)
            }
        } else if(outputDir) {
            output = new File(outputDir, inputFile.name)
        }
        return output
    }

    /**
     * creates a new filename adding the suffix before the extension
     * for example passing "inputDir/inputFile.xml" and "-Pretty" will return "inputDir/inputFile-Pretty.xml"
     */
    public static File generateSuffixedFile(File inputFile, String suffix) {
        String path = FilenameUtils.getPath(inputFile.path)
        String base = FilenameUtils.getBaseName(inputFile.name)
        String ext = FilenameUtils.getExtension(inputFile.name)
        if(ext) {
            ext = "." + ext
        }
        final String filename = base + suffix + ext
        if(inputFile.parentFile) {
            return new File(inputFile.parentFile, filename)
        } else {
            return new File(filename)
        }

    }
}
