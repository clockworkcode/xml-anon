package net.clockworkcode.prettyprint

import net.clockworkcode.anonymizer.AnonymizeConfig
import net.clockworkcode.anonymizer.Anonymizer
import net.clockworkcode.anonymizer.AnonymizerXmlEventWriter
import net.clockworkcode.anonymizer.DefaultAnonymizer

import javax.xml.stream.XMLEventWriter
import javax.xml.stream.XMLOutputFactory
import javax.xml.stream.events.XMLEvent

class PrettyPrint {
    Closure tracer

    public PrettyPrint(int traceEveryNEvents = 0) {
        if (traceEveryNEvents > 0) {
            tracer = { int eventCount, XMLEvent event ->
                if (eventCount % traceEveryNEvents == 0) {
                    print "."
                }
            }
        }
    }

    public void prettyPrint(File input, File output) {
        output.withWriter("UTF-8") { Writer writer ->
            prettyPrint(new FileReader(input), writer)
        }
    }

    public void prettyPrint(Reader input, Writer output) {
        StartDocEncodingXmlEventWriter prettyFilter = prettyPrintFilter(output)
        new XmlFilter().filter(input, prettyFilter, tracer)

    }
    public void prettyPrint(InputStream input, OutputStream output) {
        prettyPrint(new InputStreamReader(input), new OutputStreamWriter(output))

    }

//    public InputStream prettyPrint(InputStream input) {
//        PipedOutputStream startOfPipe = new PipedOutputStream()
//        PipedInputStream endOfPipe = new PipedInputStream(startOfPipe)
//        prettyPrint(new InputStreamReader(input), startOfPipe)
//        return endOfPipe
//    }

    public void anonymize(File input, File output, Anonymizer anonymizer) {
        output.withWriter("UTF-8") { Writer writer ->
            anonymize(new FileReader(input), writer, anonymizer)
        }
    }

    public void anonymize(File input, File output, File anonymizerConfig) {
        AnonymizeConfig config = new AnonymizeConfig()
        final DefaultAnonymizer anonymizer = new DefaultAnonymizer()
        config.load(new FileReader(anonymizerConfig), anonymizer)
        anonymize(input, output, anonymizer)
    }

    public void anonymize(Reader input, Writer output, Anonymizer anonymizer) {
        StartDocEncodingXmlEventWriter prettyFilter = prettyPrintFilter(output)
        AnonymizerXmlEventWriter anonFilter = new AnonymizerXmlEventWriter(prettyFilter, anonymizer)
        new XmlFilter().filter(input, anonFilter, tracer)
    }

    private StartDocEncodingXmlEventWriter prettyPrintFilter(Writer output) {
        XMLEventWriter writer = XMLOutputFactory.newInstance().createXMLEventWriter(output)
        PrettyPrintXmlEventWriter prettyWriter = new PrettyPrintXmlEventWriter(writer)
        StartDocEncodingXmlEventWriter startDocEncoding = new StartDocEncodingXmlEventWriter(prettyWriter)
        return startDocEncoding
    }
}
