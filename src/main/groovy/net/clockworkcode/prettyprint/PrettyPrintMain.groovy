package net.clockworkcode.prettyprint

class PrettyPrintMain {
    public static void main(String[] args) {
        new PrettyPrintMain().process(args)
    }

    void process(String[] args) {
        OptionAccessor options = parseOptions(args)

        if (!options) {
            println "complete"
            return
        }
        File anonymizeConfig
        if (options.f) {
            anonymizeConfig = new File(options.f)
        }
        final boolean anonymizing = options.a || options.f

        File outputFile = getOutputFile(options.i, options.d, options.o)

        runFormatter(new File(options.i), outputFile, anonymizing, anonymizeConfig)
    }

    private OptionAccessor parseOptions(String[] args) {
        CliBuilder cli = new CliBuilder(usage: "groovy PrettyPrint.groovy -i input [-o outputFile -d outputDir]")
        cli.i(argName: 'input', longOpt: 'input', args: 1, required: true, 'input file to prettyprint')
        cli.o(argName: 'outputFile', longOpt: 'outputFile', args: 1, required: false, 'optional output file')
        cli.d(argName: 'outputDir', longOpt: 'outputDir', args: 1, required: false, 'optional output directory')
        cli.f(argName: 'anonymizeConfig', longOpt: 'anonymizeWithConfig', args: 1, required: false, 'optional perform anonymizing with this config')
        cli.a(argName: 'anonymize', longOpt: 'anonymize', required: false, 'optional perform anonymizing of the xml with default config')
        def options = cli.parse(args)
        return options
    }

    File getOutputFile(def inputFile, def outputDir, def outputFile) {
        File output = CliUtil.generateOutputFilename(inputFile, outputDir, outputFile)
        if(!outputFile) {
            output = CliUtil.generateSuffixedFile(output, "-pretty")
        }
        return output
    }


    private void runFormatter(File inputFile, File outputFile, boolean anonymizing, File anonymizeConfig) {
        if (!inputFile.exists()) {
            println("could not find the input file: $inputFile.path, did you specify the full path to the input file")
        } else {
            System.out.println("outputFile = " + outputFile);
            System.out.println("inputFile = " + inputFile);
            long start = System.currentTimeMillis()
            if (anonymizing) {
                anonymize(inputFile, outputFile, anonymizeConfig)
            } else {
                prettyPrint(inputFile, outputFile)
            }
            println "\ncompleted in ${(System.currentTimeMillis() - start) / 1000} s"
        }
    }


    void prettyPrint(File input, File output) {
        println "pretty printing..."
        new PrettyPrint(100000).prettyPrint(input, output);
    }

    void anonymize(File input, File output, File anonymizeConfigFile) {
        final String scriptDir = System.getProperty("script.dir")
        if (!anonymizeConfigFile) {
            if (!scriptDir) {
                throw new RuntimeException("use the -f option or set the scriptDir system variable in the startup script.")
            } else {
                anonymizeConfigFile = new File(scriptDir, "../config/anonymize.properties")
            }
        }
        println "anonymizing using config $anonymizeConfigFile.absolutePath"
        new PrettyPrint(100000).anonymize(input, output, anonymizeConfigFile)
    }
}
