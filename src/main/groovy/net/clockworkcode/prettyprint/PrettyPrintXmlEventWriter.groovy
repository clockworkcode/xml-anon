package net.clockworkcode.prettyprint

import com.sun.xml.internal.stream.events.CharacterEvent

import javax.xml.namespace.NamespaceContext
import javax.xml.stream.XMLEventReader
import javax.xml.stream.XMLEventWriter
import javax.xml.stream.XMLStreamException
import javax.xml.stream.events.XMLEvent

class PrettyPrintXmlEventWriter implements XMLEventWriter {
    private XMLEventWriter writer;
    private int depth;
    private boolean previousEventWasCharacters = false;
    private boolean previousEventWasStart = false;

    public PrettyPrintXmlEventWriter(XMLEventWriter writer) {
        this.writer = writer;
    }

    @Override
    public void flush() throws XMLStreamException {
        writer.flush();
    }

    @Override
    public void close() throws XMLStreamException {
        writer.close();
    }

    @Override
    public void add(XMLEvent event) throws XMLStreamException {
        if (event.isStartElement()) {
//            println depth + " " + event.asStartElement().name.localPart
            previousEventWasStart = true;
            addIndent();
            depth++;
        }
        if (event.isEndElement()) {
//            println depth + " " + event.asEndElement().name.localPart
            depth--;
            if (!previousEventWasCharacters && !previousEventWasStart) {
                addIndent();
            }
            previousEventWasCharacters = false;
            previousEventWasStart = false;
        }

        if (event.isCharacters()) {
//            println depth + " " + event.asCharacters().data
            previousEventWasCharacters = true;
        }
        writer.add(event);
    }

    private void addIndent() throws XMLStreamException {
        CharacterEvent newline = new CharacterEvent("\n");
        writer.add(newline);
        CharacterEvent space = new CharacterEvent("   ");
        for (int i = 0; i < depth; i++) {
            writer.add(space);
        }
    }

    @Override
    public void add(XMLEventReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            writer.add(reader.nextEvent());
        }
    }

    @Override
    public String getPrefix(String uri) throws XMLStreamException {
        return writer.getPrefix(uri);
    }

    @Override
    public void setPrefix(String prefix, String uri) throws XMLStreamException {
        writer.setPrefix(prefix, uri);
    }

    @Override
    public void setDefaultNamespace(String uri) throws XMLStreamException {
        writer.setDefaultNamespace(uri);
    }

    @Override
    public void setNamespaceContext(NamespaceContext context) throws XMLStreamException {
        writer.setNamespaceContext(context);
    }

    @Override
    public NamespaceContext getNamespaceContext() {
        return writer.getNamespaceContext();
    }
}
