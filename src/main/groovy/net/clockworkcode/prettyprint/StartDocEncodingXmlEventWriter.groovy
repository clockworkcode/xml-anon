package net.clockworkcode.prettyprint

import javax.xml.namespace.NamespaceContext
import javax.xml.stream.XMLEventFactory
import javax.xml.stream.XMLEventReader
import javax.xml.stream.XMLEventWriter
import javax.xml.stream.XMLStreamException
import javax.xml.stream.events.XMLEvent

class StartDocEncodingXmlEventWriter implements XMLEventWriter {
    private XMLEventWriter writer;
    private int depth;
    private boolean previousEventWasCharacters = false;
    private boolean previousEventWasStart = false;
    String encoding

    public StartDocEncodingXmlEventWriter(XMLEventWriter writer, String encoding = "UTF-8") {
        this.writer = writer;
        this.encoding = encoding
    }

    @Override
    public void flush() throws XMLStreamException {
        writer.flush();
    }

    @Override
    public void close() throws XMLStreamException {
        writer.close();
    }

    @Override
    public void add(XMLEvent event) throws XMLStreamException {
        if (event.isStartDocument()) {
            event = XMLEventFactory.newInstance().createStartDocument(encoding)
        }
        writer.add(event);
    }

    @Override
    public void add(XMLEventReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            writer.add(reader.nextEvent());
        }
    }

    @Override
    public String getPrefix(String uri) throws XMLStreamException {
        return writer.getPrefix(uri);
    }

    @Override
    public void setPrefix(String prefix, String uri) throws XMLStreamException {
        writer.setPrefix(prefix, uri);
    }

    @Override
    public void setDefaultNamespace(String uri) throws XMLStreamException {
        writer.setDefaultNamespace(uri);
    }

    @Override
    public void setNamespaceContext(NamespaceContext context) throws XMLStreamException {
        writer.setNamespaceContext(context);
    }

    @Override
    public NamespaceContext getNamespaceContext() {
        return writer.getNamespaceContext();
    }
}
