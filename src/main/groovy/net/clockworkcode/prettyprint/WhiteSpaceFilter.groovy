package net.clockworkcode.prettyprint

import javax.xml.stream.EventFilter
import javax.xml.stream.events.Characters
import javax.xml.stream.events.XMLEvent

class WhiteSpaceFilter implements EventFilter {
    @Override
    public boolean accept(XMLEvent event) {
        if (event.isCharacters() &&
            (((Characters) event).isIgnorableWhiteSpace() || ((Characters) event).isWhiteSpace())) {
            return false;
        } else {
            return true;
        }
    }

}
