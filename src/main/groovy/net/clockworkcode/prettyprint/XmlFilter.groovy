package net.clockworkcode.prettyprint

import javax.xml.stream.EventFilter
import javax.xml.stream.XMLEventReader
import javax.xml.stream.XMLEventWriter
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.events.XMLEvent

/**
 * Created: 24/05/11
 * @author david
 */
public class XmlFilter {
    public void filter(Reader input, XMLEventWriter xmlFilter, EventFilter filter, Closure trace, String encoding = "UTF-8") {
        int count = 0

        XMLInputFactory factory = XMLInputFactory.newInstance()
        factory.setProperty(XMLInputFactory.IS_COALESCING, Boolean.TRUE)
        XMLEventReader eventReader = factory.createXMLEventReader(input)
        XMLEventReader filteredReader = factory.createFilteredReader(eventReader, filter)
        while (filteredReader.hasNext()) {
            count++
            XMLEvent e = filteredReader.nextEvent()
//            println e
            xmlFilter.add(e)
            if(trace) {
                trace(count, e)
            }
        }
//        println "-----------------------------"
        xmlFilter.flush()
    }

    public void filter(Reader input, XMLEventWriter xmlFilter, Closure trace, String encoding = "UTF-8") {
        filter(input, xmlFilter, new WhiteSpaceFilter(), trace, encoding)
    }
}