package net.clockworkcode.anonymize

import groovy.mock.interceptor.StubFor
import net.clockworkcode.anonymizer.AnonymizeConfig
import net.clockworkcode.anonymizer.DefaultAnonymizer
import org.junit.Test

class AnonymizeConfigTest {
    String configString =
    """/People/Person/Address/AddressLine1=13 The Street,14 The Street,15 The Street
Forename=David,Mark,Arthur
/Details/AccountNumber=type=random
Value=type=random,min=21,max=99"""

    @Test
    public void testCompleteOperations() {
        def mock = new StubFor(DefaultAnonymizer)
        Map expected =
         ["/People/Person/Address/AddressLine1":["13 The Street", "14 The Street", "15 The Street"],
         "Forename": ["David", "Mark", "Arthur"],
         "/Details/AccountNumber": [type: "random"],
         "Value": [type: "random", min:"21", max:"99"]]

        mock.demand.setOptions(2) { String path, Map<String,String> options ->
            assert expected[path] == options
        }
        mock.demand.setChoices(2) { String path, List<String> choices ->
            assert expected[path] == choices
        }
        mock.use {
            AnonymizeConfig config = new AnonymizeConfig()
            config.load(new StringReader(configString), new DefaultAnonymizer())
        }
    }
}
