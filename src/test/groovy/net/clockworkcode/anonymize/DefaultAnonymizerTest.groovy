package net.clockworkcode.anonymize

import net.clockworkcode.anonymizer.Anonymizer
import net.clockworkcode.anonymizer.DefaultAnonymizer
import org.junit.Test

import static junit.framework.Assert.assertFalse
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

class DefaultAnonymizerTest {
    @Test
    public void testPathOrders() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        anonymizer.setChoices("addressline", ["David"])
        anonymizer.setChoices("address.*", ["Mark"])
        assertEquals("David", anonymizer.anonymize("/People/Person/Address/AddressLine1", "AddressLine", "Jack"))
    }
    @Test
    public void testPathOrdersReversed() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        anonymizer.setChoices("address.*", ["Mark"])
        anonymizer.setChoices("addressline", ["David"])
        assertEquals("Mark", anonymizer.anonymize("/People/Person/Address/AddressLine1", "AddressLine", "Jack"))
    }
    @Test
    public void testRegExprIgnoreCase() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        final List<String> choices = ["David", "Mark", "Arthur"]
        anonymizer.setChoices("addressline.*", choices)
        assertTrue(choices.contains(anonymizer.anonymize("/People/Person/Address/AddressLine1", "AddressLine1", "Jack")))
    }

    public void testRegExpr() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        final List<String> choices = ["David", "Mark", "Arthur"]
        anonymizer.setChoices("AddressLine.*", choices)
        assertTrue(choices.contains(anonymizer.anonymize("/People/Person/Address/AddressLine1", "AddressLine1", "Jack")))
    }

    @Test
    public void testElementNoPath() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        final List<String> choices = ["David", "Mark", "Arthur"]
        anonymizer.setChoices("/Person", choices)
        assertTrue(choices.contains(anonymizer.anonymize("/People/Person", "Person", "Jack")))
    }

    @Test
    public void testNoMatchNullAttribute() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        final List<String> choices = ["David", "Mark", "Arthur"]
        anonymizer.setChoices("Age", choices)
        assertFalse(choices.contains(anonymizer.anonymize("/People/Person", "Person", null, "Jack")))
    }

    @Test
    public void testAttributeXPathConfig() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        final List<String> choices = ["David", "Mark", "Arthur"]
        anonymizer.setChoices("/People/Person@Forename", choices)
        assertTrue(choices.contains(anonymizer.anonymize("/People/Person", "Person", "Forename", "Jack")))
    }

    @Test
    public void testAttributeNoPath() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        final List<String> choices = ["David", "Mark", "Arthur"]
        anonymizer.setChoices("@Forename", choices)
        assertTrue(choices.contains(anonymizer.anonymize("/People/Person", "Person", "Forename", "Jack")))
    }

    @Test
    public void testXPathConfig() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        final List<String> choices = ["13 The Street", "14 The Street", "15 The Street"]
        anonymizer.setChoices("/People/Person/Address/AddressLine1", choices)
        assertTrue(choices.contains(anonymizer.anonymize("/People/Person/Address/AddressLine1", "AddressLine1", "1 High Street")))
    }

    @Test
    public void testPartialXPathConfig() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        final List<String> choices = ["David", "Mark", "Arthur"]
        anonymizer.setChoices("Forename", choices)
        assertTrue(choices.contains(anonymizer.anonymize("/Person/Forename", "Forename", "1 High Street")))
    }

    @Test
    public void testEmptyConfigValuesNumeric() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        anonymizer.setOptions("AccountNumber", [type:"random"])
        String inputAccountNumber = "1234567890"
        String anonymizedAccount = anonymizer.anonymize("/People/Person/AccountNumber", "AccountNumber", inputAccountNumber)
        assertFalse(anonymizedAccount.equals(inputAccountNumber))
        assertEquals(inputAccountNumber.size(), anonymizedAccount.size())
        assertTrue(anonymizedAccount.isNumber())
    }

    @Test
    public void testEmptyConfigValuesFloat() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        anonymizer.setOptions("Value", [type:"random"])
        String inputAmount = "1234.67890"
        String anonymizedAmount = anonymizer.anonymize("/People/Person/Value", "Value", inputAmount)
        assertFalse(anonymizedAmount.equals(inputAmount))
        assertEquals(inputAmount.size(), anonymizedAmount.size())
        assertTrue(anonymizedAmount.isNumber())
        assertEquals(inputAmount.indexOf('.'), anonymizedAmount.indexOf('.'))
    }

    @Test
    public void testEmptyConfigValuesAlpha() {
        Anonymizer anonymizer = new DefaultAnonymizer()
        anonymizer.setOptions("ProductCode", [type:"random"])
        String inputProductCode = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        String anonymizedProductCode = anonymizer.anonymize("/People/Person/ProductCode", "ProductCode", inputProductCode)
        assertFalse(anonymizedProductCode.equals(inputProductCode))
        assertEquals(inputProductCode.size(), anonymizedProductCode.size())
        anonymizedProductCode.getChars().each { Character letter ->
            assertTrue(letter.isLetter())
        }
    }
}
