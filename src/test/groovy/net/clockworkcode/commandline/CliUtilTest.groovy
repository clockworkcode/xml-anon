package net.clockworkcode.commandline

import org.junit.Test
import org.apache.commons.io.FilenameUtils
import static org.junit.Assert.assertTrue

/**
 * Created: 27/05/11
 * @author david
 */
class CliUtilTest {
    @Test
    public void testGenerateOutputFilename() {
        checkGenerateOutputFilename("-i inputDir/inputFile.xml", "inputDir/inputFile.xml")
        checkGenerateOutputFilename("-i inputDir/inputFile.xml -o output/outputFile.xml","output/outputFile.xml")
        checkGenerateOutputFilename("-i inputDir/inputFile.xml -d outputDir","outputDir/inputFile.xml")
        checkGenerateOutputFilename("-i inputDir/inputFile.xml -d outputDir -o output/outputFile.xml","outputDir/output/outputFile.xml")
        checkGenerateOutputFilename("-i inputDir/inputFile.xml -d outputDir -o outputFile.xml","outputDir/outputFile.xml")
    }

    @Test
    public void testGenerateSuffixedFile() {
        checkGenerateSuffixedFile("inputDir/inputFile.xml", "-pretty", "inputDir/inputFile-pretty.xml")
        checkGenerateSuffixedFile("inputDir/inputFile", "-pretty", "inputDir/inputFile-pretty")
        checkGenerateSuffixedFile("inputFile.xml", "-pretty", "inputFile-pretty.xml")
    }

    void checkGenerateSuffixedFile(String inputFilename, String suffix, String expected) {
        File output = CliUtil.generateSuffixedFile(new File(inputFilename), suffix)
        final String message = "expected ($expected) output (${output.path})"
        org.junit.Assert.assertTrue(message, FilenameUtils.equals(expected, output.path))
    }


    void checkGenerateOutputFilename(String commandLine, String expected) {
        String[] args = commandLine.split()
        OptionAccessor options = parseOptions(args)
        File output = CliUtil.generateOutputFilename(options.i, options.d, options.o)
        final String message = "expected:$expected output:${output.path}"
        org.junit.Assert.assertTrue(message, FilenameUtils.equals(expected, output.path))
    }

    public static OptionAccessor parseOptions(String[] args) {
        CliBuilder cli = new CliBuilder(usage: "groovy PrettyPrint.groovy -i input [-o outputFile -d outputDir]")
        cli.i(argName: 'input', longOpt: 'input', args: 1, required: true, 'input file')
        cli.o(argName: 'outputFile', longOpt: 'outputFile', args: 1, required: false, 'optional output file')
        cli.d(argName: 'outputDir', longOpt: 'outputDir', args: 1, required: false, 'optional output directory')
        def options = cli.parse(args)
        return options
    }

}
