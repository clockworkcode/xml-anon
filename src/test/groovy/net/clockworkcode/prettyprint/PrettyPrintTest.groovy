package net.clockworkcode.prettyprint

import net.clockworkcode.anonymizer.AnonymizeConfig
import net.clockworkcode.anonymizer.DefaultAnonymizer
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals

// some good doco on stax - http://java.sun.com/webservices/reference/tutorials/jaxp/html/stax.html
class PrettyPrintTest {
    @Before
    public void setup() {
        System.setProperty("script.dir", "/temp")
    }
    @Test
    public void testPrettyPrint() {
        // TODO put empty element e.g. <x/> to test this
        String inputString = """<?xml version="1.0" encoding="UTF-8"?><root><name first="david" last="smith"></name>
  <colour>Red</colour>  <a>   <b>banana boat</b>  </a></root>"""

        String expected = """<?xml version="1.0" encoding="UTF-8"?>
<root>
   <name last="smith" first="david"></name>
   <colour>Red</colour>
   <a>
      <b>banana boat</b>
   </a>
</root>"""

        StringReader input = new StringReader(inputString)
        StringWriter output = new StringWriter()
        PrettyPrint prettyPrinter = new PrettyPrint();
        prettyPrinter.prettyPrint(input, output)
        assertEquals(expected, output.toString())
    }

    @Test
    public void testAnonymizer() {
        String configString =
        """last=Bloggs
colour=Green
age=type=random"""

        String inputString = """<?xml version="1.0" encoding="UTF-8"?><root xmlns:abc="http://example.com/schemamain">
        <ns:name xmlns:ns="http://example.org/schema" first="david" last="smith"></ns:name>
  <Colour>Red</Colour>  <a>   <b>banana boat</b>  </a></root>"""

        String expected = """<?xml version="1.0" encoding="UTF-8"?>
<root xmlns:abc="http://example.com/schemamain">
   <ns:name xmlns:ns="http://example.org/schema" last="Bloggs" first="david"></ns:name>
   <Colour>Green</Colour>
   <a>
      <b>banana boat</b>
   </a>
</root>"""
        AnonymizeConfig config = new AnonymizeConfig()
        final DefaultAnonymizer anonymizer = new DefaultAnonymizer()
        config.load(new StringReader(configString), anonymizer)
        StringReader input = new StringReader(inputString)
        StringWriter output = new StringWriter()
        PrettyPrint prettyPrinter = new PrettyPrint();
        prettyPrinter.anonymize(input, output, anonymizer)
        assertEquals(expected, output.toString())
    }
}
